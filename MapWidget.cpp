﻿#include "stdafx.h"
#include "MapWidget.h"
#include "task.h"

//Thanks to https://github.com/mhogomchungu/tasks
//painting in other thread http://doc.qt.io/qt-5/qtcore-threads-mandelbrot-example.html

class CustomEvent : public QEvent
{
	osmscout::GeoCoord _coord;
	double _compass;
public:
	explicit CustomEvent(const osmscout::GeoCoord& coord, const double& compass) : QEvent(static_cast<Type>(User + 1)) {
		_coord = coord;
		_compass = compass;
	}

	osmscout::GeoCoord GetCoord() const {
		return _coord;
	}

	double GetCompass() const {
		return _compass;
	}
};

void MapWidget::ContextMenuRequested(const QPoint& pos) {

	if(_libOsmScout == nullptr) return;

	auto globalPos = this->mapToGlobal(pos);
	QMenu myMenu;
	auto startAction = myMenu.addAction(tr("Als Startpunkt nehmen"), this, &MapWidget::SetRouteStart);
	startAction->setData(pos);
	auto targetAction = myMenu.addAction(tr("Als Zielpunkt nehmen"), this, &MapWidget::SetRouteTarget);
	targetAction->setData(pos);
	myMenu.exec(globalPos);
}

void MapWidget::SetRouteStart() {
	auto act = qobject_cast<QAction *>(sender());
	auto point = act->data().toPoint();

	qDebug() << "Use als start at" << point.x() << " x " << point.y();

	auto start = _libOsmScout->GetCoordFromPixel(point);
	emit setRouteStartSignal(start);
}

void MapWidget::SetRouteTarget() {
	auto act = qobject_cast<QAction *>(sender());
	auto point = act->data().toPoint();

	qDebug() << "Use als target at" << point.x() << " x " << point.y();

	auto target = _libOsmScout->GetCoordFromPixel(point);
	emit setRouteTargetSignal(target);
}

MapWidget::MapWidget(QWidget *parent) :
	QWidget(parent) {

	resize(parent->size());
	_libOsmScout = nullptr;
	updateRunning = false;
	_mapMove.setX(0);
	_mapMove.setY(0);
	_mapMoveThreadStart.setX(0);
	_mapMoveThreadStart.setY(0);
	//setMouseTracking
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QListWidget::customContextMenuRequested, this, &MapWidget::ContextMenuRequested);
	//connect(this, &MapWidget::setCenterMapSignal, this, &MapWidget::CenterMapSignal);
	_guithread = QThread::currentThread();
}

bool MapWidget::event(QEvent* evt) {
	if (evt->type() == QEvent::User + 1) {
		qDebug() << "Event received in thread" << QThread::currentThread();
		auto customEvent = static_cast<CustomEvent*>(evt);
		CenterMap(customEvent->GetCoord(), customEvent->GetCompass());
		return true;
	}
	return QWidget::event(evt);
}

void MapWidget::paintEvent(QPaintEvent* event) {
	//https://stackoverflow.com/questions/18959083/qpainter-drawimage-centeraligned

	QPainter painter;
	painter.begin(this);
	
	if (_currentMap.isNull())
	{
		auto font = painter.font();
		font.setPointSize(18);
		painter.setFont(font);
		painter.setPen(Qt::black);
		QFontMetrics fm(font);
		auto width = fm.width("No map loaded");
		auto height = fm.height();
		painter.fillRect(event->rect(), Qt::red);
		painter.drawText(event->rect().width() / 2 - (width / 2), event->rect().height() / 2 - (height / 2), "No map loaded");
		//QWidget::paintEvent(event);
	} else {
		osmscout::StopClock drawTimer;
		painter.fillRect(event->rect(), Qt::green);
		_mapDataLock.lock();
		QRectF target(_mapMove.x(), _mapMove.y(), event->rect().width(), event->rect().height());
		QRectF source(0, 0, _currentMap.size().width(), _currentMap.size().height());
		painter.drawPixmap(target, _currentMap, source);
		drawTimer.Stop();
		_mapDataLock.unlock();

		auto dbTime = drawTimer.GetMilliseconds();
		qDebug() << dbTime << "ms to draw on screen";
	}

	painter.end();
}

void MapWidget::resizeEvent(QResizeEvent* event) {
	qDebug() << "resize to " << event->size().width() << " x " << event->size().height();
	QWidget::resizeEvent(event);
	
	if (_libOsmScout == nullptr) return;

	if(!updateRunning) {
		updateRunning = true;
		_libOsmScout->UpdateSize(size());
	}
}

void MapWidget::mousePressEvent(QMouseEvent* event) {
	qDebug() << "mouse down on " << event->x() << " x " << event->y() << " botton " << event->button();
	if (event->button() == Qt::LeftButton) {
		_mouseDownAt = event->pos();
		event->accept();
	}
	
}

void MapWidget::mouseReleaseEvent(QMouseEvent* event) {
	qDebug() << "mouse up on " << event->x() << " x " << event->y() << " botton " << event->button();

	if (event->button() == Qt::LeftButton) {
		//MoveMap(event->x() - _mouseDownAt.x(), event->y() - _mouseDownAt.y());
		event->accept();
	}
}

void MapWidget::mouseMoveEvent(QMouseEvent* event) {
	qDebug() << "mouse move to " << event->x() << " x " << event->y() << " botton " << event->button();

	_mapMove.setX(_mapMove.x() + (event->x() - _mouseDownAt.x()));
	_mapMove.setY(_mapMove.y() + (event->y() - _mouseDownAt.y()));
	update();
	
	_mouseDownAt = event->pos();

	if (!updateRunning) {
		updateRunning = true;
		_mapMoveThreadStart.setX(_mapMove.x());
		_mapMoveThreadStart.setY(_mapMove.y());
		Task::future<void>& drawTask = Task::run([this]{ MoveMap(_mapMoveThreadStart.x(), _mapMoveThreadStart.y()); });
		drawTask.then([this] {RealFinish(); });
	}
	event->accept();
}

void MapWidget::wheelEvent(QWheelEvent* event) {
	emit updateStatusLineSignal("Map loading...");
	auto numPixels = event->pixelDelta();
	auto numDegrees = event->angleDelta() / 8;
	auto update = false;
	if (!numPixels.isNull()) {
		if (numPixels.y() > 0) {
			update = _libOsmScout->ZoomUp();
		}
		else {
			update = _libOsmScout->ZoomDown();
		}
	}
	if (!numDegrees.isNull()) {
		if (numDegrees.y() > 0) {
			update = _libOsmScout->ZoomUp();
		}
		else {
			update = _libOsmScout->ZoomDown();
		}
	}
	if (!update) {
		emit updateStatusLineSignal("Map loaded");
	} 
	event->accept();
}

void MapWidget::MoveMap(int diffx, int diffy) {
	if (diffx == 0 && diffy == 0) return;

	qDebug() << "move map " << diffx << " x " << diffy;
	emit updateStatusLineSignal("Map loading...");
	_libOsmScout->MoveMap(-diffx, diffy);
	
}

void MapWidget::SetMapHelper(LibOsmScoutHelper* libOsmScout) {
	_libOsmScout = libOsmScout;

	//todo get forma that belongs to screen
	//https://stackoverflow.com/questions/31266759/qt5-how-to-determine-screen-pixel-format
		
	updateRunning = true;
	auto delegateUpdate = std::bind(&MapWidget::UpdateMapImage, this, std::placeholders::_1);
	auto delegateFinish = std::bind(&MapWidget::UpdateMapImageFinish, this);
	_libOsmScout->RegisterCallback(delegateUpdate, delegateFinish);

	_libOsmScout->UpdateSize(size());
}

void MapWidget::UpdateMapImage(const QImage& image) {
	_mapDataLock.lock();
	_currentMap = QPixmap::fromImage(image);
	_mapDataLock.unlock();

	_mapMove.setX(_mapMove.x() - _mapMoveThreadStart.x());
	_mapMove.setY(_mapMove.y() - _mapMoveThreadStart.y());
	qDebug() << "move map " << _mapMove.x() << " x " << _mapMove.y();
	update();
}

void MapWidget::UpdateMapImageFinish() {
	if (_guithread == QThread::currentThread())
	{
		qDebug() << "UpdateMapImageFinish";
		
		_mapMove.setX(0);
		_mapMove.setY(0);
		_mapMoveThreadStart.setX(0);
		_mapMoveThreadStart.setY(0);
		emit updateStatusLineSignal("Map loaded");

		QString streetName;
		if(_libOsmScout->FindWayForCenter(streetName)) {
			emit updateStatusLineSignal(streetName);
		}
		updateRunning = false;
	}
}

void MapWidget::RealFinish() {
	qDebug() << "RealFinish";
	if (_mapMove.x() == 0 && _mapMove.y() == 0) {
		_mapMoveThreadStart.setX(0);
		_mapMoveThreadStart.setY(0);
		emit updateStatusLineSignal("Map loaded");
		QString streetName;
		if (_libOsmScout->FindWayForCenter(streetName)) {
			emit updateStatusLineSignal(streetName);
		}
		updateRunning = false;
	}
	else {
		_mapMoveThreadStart.setX(_mapMove.x());
		_mapMoveThreadStart.setY(_mapMove.y());

		Task::future<void>& drawTask = Task::run([this] { MoveMap(_mapMoveThreadStart.x(), _mapMoveThreadStart.y()); });
		drawTask.then([this] {RealFinish(); });
	}
}

void MapWidget::CenterMap(const osmscout::GeoCoord& coord, const double& compass) {
	if(_libOsmScout == nullptr) return;
	if (_guithread != QThread::currentThread())
	{
		auto userEvent = new CustomEvent(coord, compass);
		QCoreApplication::postEvent(this, userEvent);
		return;
	}

	if (updateRunning) return;
	
	updateRunning = true;
	
	qDebug() << "CenterMap";
	Task::future<void>& drawTask = Task::run([this, coord, compass] {_libOsmScout->CenterMap(coord, true, compass); });
	drawTask.then([this] {RealFinish(); });
}




