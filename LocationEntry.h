﻿#pragma once
#include <osmscout/GeoCoord.h>
#include <osmscout/util/GeoBox.h>

class LocationEntry {
	QString                        _label;
	osmscout::GeoCoord             _coord;
	osmscout::GeoBox               _bbox;
public:
	LocationEntry();
	LocationEntry(QString label, osmscout::GeoCoord coord, osmscout::GeoBox bbox);

	QString Label();
	osmscout::GeoCoord Coord();
	osmscout::GeoBox Box();
};

Q_DECLARE_METATYPE(LocationEntry)