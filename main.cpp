#include "stdafx.h"
#include "QtMapPainterWithLibOsmScout.h"
#include <QtWidgets/QApplication>

#define MY_VERSION_STR "0.0.1"

int main(int argc, char *argv[])
{
	//http://doc.qt.io/qt-5/resources.html
	Q_INIT_RESOURCE(QtMapPainterWithLibOsmScout);
	QApplication app(argc, argv);

	QtMapPainterWithLibOsmScout mainWindow;

	QCoreApplication::setOrganizationName("MiNeSoft");
	QCoreApplication::setOrganizationDomain("mine-robo.de");
	QCoreApplication::setApplicationName("Show an Map with OSMScout");
	QCoreApplication::setApplicationVersion(MY_VERSION_STR);

	//Damit man auf der Comando zeile ein Paar Info �ber die Applikation bekommt
	//https://doc.qt.io/qt-5/qcommandlineparser.html
	QCommandLineParser parser;
	parser.setApplicationDescription(QCoreApplication::applicationName());
	parser.addHelpOption();
	parser.addVersionOption();
	parser.process(app);

	//Hauptfenster an zeigen
	mainWindow.show();
	return app.exec();
}
