﻿#include "stdafx.h"
#include "LibOsmScoutHelper.h"
#include "SomeThingWrong.h"
#include <osmscout/TextSearchIndex.h>
#include <osmscout/LocationDescriptionService.h>
#include <osmscout/routing/RoutingService.h>
#include <osmscout/routing/SimpleRoutingService.h>
#include "QtRoutingProgress.h"

//Helper Class for the Libosmscout
// All Funktions are Sync so the GUI is NOT resposiv but the source is better to understand

//Todo move to Config File
static void GetCarSpeedTable(std::map<std::string, double>& map)
{
	map["highway_motorway"] = 110.0;
	map["highway_motorway_trunk"] = 100.0;
	map["highway_motorway_primary"] = 70.0;
	map["highway_motorway_link"] = 60.0;
	map["highway_motorway_junction"] = 60.0;
	map["highway_trunk"] = 100.0;
	map["highway_trunk_link"] = 60.0;
	map["highway_primary"] = 70.0;
	map["highway_primary_link"] = 60.0;
	map["highway_secondary"] = 60.0;
	map["highway_secondary_link"] = 50.0;
	map["highway_tertiary_link"] = 55.0;
	map["highway_tertiary"] = 55.0;
	map["highway_unclassified"] = 50.0;
	map["highway_road"] = 50.0;
	map["highway_residential"] = 40.0;
	map["highway_roundabout"] = 40.0;
	map["highway_living_street"] = 10.0;
	map["highway_service"] = 30.0;
}

LibOsmScoutHelper::LibOsmScoutHelper() :
	_mapPainter(nullptr), _titleCallBackId(-1),
	_zoomLevel(15),
	_dpi(0),
	_width(0),
	_height(0),
	_firstloadDone(false),
	_paintMarker(false), 
	_compass(0),
	_center(50.0908, 8.50699) {
	_databaseDirectory = "";
	_markerPos.Set(-1, -1);
	_markerImage = nullptr;
	_stylesheetFlags["daylight"] = true;
	_useMarkerImage = true;
}


void LibOsmScoutHelper::OpenDatabase(const QString& directory) {
	osmscout::DatabaseParameter databaseParameter;

	qDebug("we need minimal db version %d max dbversion %d", osmscout::TypeConfig::MIN_FORMAT_VERSION, osmscout::TypeConfig::MAX_FORMAT_VERSION);

	QLocale locale;
	qDebug("test %s", locale.name().toStdString().c_str());
	_databaseDirectory = directory;

	_database.reset(new osmscout::Database(databaseParameter));
	_mapService.reset(new osmscout::MapService(_database));
	if(!_database->Open(directory.toStdString())) {
		SomeThingWrongException exp("Failed to Open Database");
		exp.raise();
	}
	//_mapService->SetCacheSize(10000);

	qDebug() << "Database loaded\n";
}

void LibOsmScoutHelper::OpenStyle(const QString& file) {
	_styleConfig.reset(new osmscout::StyleConfig(_database->GetTypeConfig()));
	for (const auto& flag : _stylesheetFlags) {
		_styleConfig->AddFlag(flag.first, flag.second);
	}
	if (!_styleConfig->Load(file.toStdString())) {
		SomeThingWrongException exp("Fail to open Style");
		exp.raise();
	}
	_sytleFile = file;
}

void LibOsmScoutHelper::BuildingMap(osmscout::GeoCoord center, int zoomLevel, const std::vector<std::string>& iconPath) {

	auto appHasQtWidgetsSupport = qobject_cast<QApplication *>(QCoreApplication::instance());
	Q_ASSERT(appHasQtWidgetsSupport);
	Q_UNUSED(appHasQtWidgetsSupport);
	
	_mapPainter = new osmscout::MapPainterQt(_styleConfig);
	_dpi = appHasQtWidgetsSupport->screens().at(appHasQtWidgetsSupport->desktop()->primaryScreen())->physicalDotsPerInch();

	//Standard schrift grösse
	_drawParameter.SetFontSize(3.0);
	_drawParameter.SetDebugPerformance(true);

	// optimize process can reduce number of nodes before rendering
	// it helps for slow renderer backend, but it cost some cpu
	// it seems that it is ok to disable it for Qt
	_drawParameter.SetOptimizeWayNodes(osmscout::TransPolygon::none);
	_drawParameter.SetOptimizeAreaNodes(osmscout::TransPolygon::none);
	
	//Where Map Picture are found
	std::list<std::string> paths;
	std::copy(iconPath.begin(), iconPath.end(), std::back_inserter(paths));
	
	_drawParameter.SetIconPaths(paths);
	_drawParameter.SetPatternPaths(paths);

	//Karten ausschnitt den man sehen will
	//Mittelpunkt der karte Zoom Stufe bild größe
	//Zoom 0 - 20
	if (center.GetLat() > 0 && center.GetLat() < 90)
	{
		_center = center;
	}
	_zoomLevel = zoomLevel;
	_magnification.SetLevel(_zoomLevel);

}

void LibOsmScoutHelper::RenderOnPainter(QPainter* painter) const {

	osmscout::StopClock drawTimer;
	//Karte mit dem Painter zeichnen
	if (!_mapPainter->DrawMap(_projection,
		_drawParameter,
		_data,
		painter)) {
		SomeThingWrongException exp("Fail draw map");
		exp.raise();
	}
	
	drawTimer.Stop();
	auto dbTime = drawTimer.GetMilliseconds();
	qDebug() << dbTime << "ms to draw data";
}

void LibOsmScoutHelper::RenderBaseMap() {
	osmscout::StopClock drawTimer;
	//Todo make realy base map not hole map

	QPainter painter;
	painter.begin(&_currentMap);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.setRenderHint(QPainter::TextAntialiasing);
	painter.setRenderHint(QPainter::SmoothPixmapTransform);

	_mapDataLock.lock();
	if (!_mapPainter->DrawMap(_projection,
		_drawParameter,
		_data,
		&painter)) {
		SomeThingWrongException exp("Fail to draw map");
		exp.raise();
	}
	_mapDataLock.unlock();

	if(_lastRoute.size() > 0) {
		auto green = QColor::fromRgbF(0, 1, 0, 0.8);
		QPen pen;
		pen.setWidth(5);
		pen.setColor(green);
		painter.setPen(pen);
		for (auto i = 0; i < _lastRoute.size() - 1; ++i) {
			double x, y, x2, y2;
			_projection.GeoToPixel(_lastRoute[i], x, y);
			_projection.GeoToPixel(_lastRoute[i+1], x2, y2);
			painter.drawLine(x, y, x2, y2);
		}
	}

	if(_paintMarker) {
		double x, y;
		if (_markerPos.GetLat() != -1) {
			_projection.GeoToPixel(_markerPos, x, y);
		}
		else {
			_projection.GeoToPixel(_projection.GetCenter(), x, y);
		}
		auto center = QPointF(x, y);

		if (!_useMarkerImage)
		{
			auto green = QColor::fromRgbF(0, 1, 0, 0.8);
			QPen pen;
			pen.setWidth(5);
			pen.setColor(green);
			painter.setPen(pen);
			painter.setBrush(Qt::NoBrush);
			painter.drawEllipse(center, 15, 15);
		}
		else
		{
			if (_markerImage == nullptr)
			{
				if (_stylesheetFlags["daylight"]) {
					_markerImage = new QImage(":/QtMapPainterWithLibOsmScout/Resources/auto-day-icon.png");
				}
				else {
					_markerImage = new QImage(":/QtMapPainterWithLibOsmScout/Resources/auto-night-icon.png");
				}
			}
			center.setX(center.x() - (_markerImage->size().width() / 2));
			center.setY(center.y() - (_markerImage->size().height() / 2));
			painter.drawImage(center, *_markerImage);
		}
	}
	painter.end();

	drawTimer.Stop();
	auto dbTime = drawTimer.GetMilliseconds();
	qDebug() << dbTime << "ms to draw data";

	if (_MapImageUpdate) {
		_MapImageUpdate(_currentMap);
	}
}

void LibOsmScoutHelper::UpdateSize(const QSize& size) {
	UpdateSize(size.width(), size.height());
}

void LibOsmScoutHelper::UpdateMapData() {
	
	osmscout::StopClock clearTimer;
	//if not do this the map painter slow down !!!
	//but the loading the data slow down.
	
	_mapDataLock.lock();
	ClearUnusedData();
	_mapDataLock.unlock();

	qDebug() << clearTimer.GetMilliseconds() << "ms to clear database";

	_projection.SetLinearInterpolationUsage(_magnification.GetLevel() >= 10);

	osmscout::StopClock titlesTimer;
	osmscout::AreaSearchParameter searchParameter;
	searchParameter.SetUseMultithreading(true);

	if (_magnification.GetLevel() >= 15) {
		searchParameter.SetMaximumAreaLevel(6);
	}
	else {
		searchParameter.SetMaximumAreaLevel(4);
	}

	auto count = 0;
	_mapService->LookupTiles(_projection, _tiles);
	for (auto it = _tiles.begin(); it != _tiles.end(); ++it) {
		if (!it->get()->IsComplete()) {
			count++;
		}
	}
	
	qDebug() << titlesTimer.GetMilliseconds() << "ms to find titles " << _tiles.size() << " size need load " << count;

	osmscout::StopClock dbTimer;

	if (!_mapService->LoadMissingTileData(searchParameter, *_styleConfig, _tiles)) {
		//Laden der Karten daten ging schief
		SomeThingWrongException exp("Error loading TileData");
		exp.raise();
	}
	/*_firstloadDone = false;
	if (_titleCallBackId == -1)
	{
		osmscout::MapService::TileStateCallback callback = [this](const osmscout::TileRef& tile) {
			
			if(!_firstloadDone) return;
			if (!tile->IsComplete()) return;

			auto count = 0;
			for (auto &tileEntry : _tiles) {
				if (!tileEntry->IsComplete()) {
					count++;
				}
			}
			qDebug() << "tile loaded todo " << count;
			if (_mapDataLock.try_lock())
			{
				qDebug() << "add data to map";
				ClearUnusedData();
				_mapService->AddTileDataToMapData(_tiles, _data);
				_mapDataLock.unlock();
				RenderBaseMap();
			}

			//RenderBaseMap();
			//std::cout << "callback called for job: " << this << std::endl;
			//emit tileStateChanged(path, tile);
		};
		
		_titleCallBackId = _mapService->RegisterTileStateCallback(callback);
	}

	if (!_mapService->LoadMissingTileDataAsync(searchParameter,	*_styleConfig, _tiles)) {
		//Laden der Karten daten ging schief
		SomeThingWrongException exp("Error laoding TileData");
		exp.raise();
	}*/

	_mapDataLock.lock();
	_mapService->AddTileDataToMapData(_tiles, _data);
	_mapDataLock.unlock();
		
	_firstloadDone = true;

	dbTimer.Stop();

	auto dbTime = dbTimer.GetMilliseconds();
	qDebug() << dbTime << "ms to load data";

	RenderBaseMap();

	if (_MapImageFinish) {
		_MapImageFinish();
	}
}

void LibOsmScoutHelper::UpdateSize(int width, int height) {
	if(_width == width && _height == height) return;
	_currentMap = QImage(width, height, QImage::Format_ARGB32);
	_width = width;
	_height = height;
	_projection.Set(_center,
		_magnification,
		_dpi,
		width,
		height);
	UpdateMapData();
}

size_t LibOsmScoutHelper::SearchFor(const QString& text, QList<LocationEntry>& resultText) {
	osmscout::TextSearchIndex textSearch;

	if (!textSearch.Load(_databaseDirectory.toStdString())) {
		SomeThingWrongException exp("Failed to load serach index");
		exp.raise();
		return -1;
	}
	
	// search using the text input as the query
	osmscout::TextSearchIndex::ResultsMap results;

	textSearch.Search(osmscout::LocaleStringToUTF8String(text.toStdString()), true, true, true, true, results);
	if (results.empty()) {
		return 0;
	}

	size_t printCount = 0;
	for (auto it = results.begin(); it != results.end(); ++it) {
		qDebug() << "\"" << it->first.c_str() << "\" -> ";
		auto& refs = it->second;
		std::size_t maxPrintedOffsets = 5;
		auto minRefCount = std::min(refs.size(), maxPrintedOffsets);

		osmscout::GeoCoord coordinates;
		osmscout::GeoBox bbox;

		for (size_t r = 0; r < minRefCount; r++) {
			if (refs[r].GetType() == osmscout::refNode) {
				qDebug() << " * N:" << refs[r].GetFileOffset();
				osmscout::NodeRef node;
				if (_database->GetNodeByOffset(refs[r].GetFileOffset(), node)) {
					GetObject(node->GetFeatureValueBuffer(), coordinates, bbox);
					coordinates = node->GetCoords();
				}
			}
			else if (refs[r].GetType() == osmscout::refWay) {
				qDebug() << " * W:" << refs[r].GetFileOffset();
				osmscout::WayRef way;
				if (_database->GetWayByOffset(refs[r].GetFileOffset(), way)) {
					GetObject(way->GetFeatureValueBuffer(), coordinates, bbox);
					way->GetCenter(coordinates);
				}
			}
			else if (refs[r].GetType() == osmscout::refArea) {
				qDebug() << " * A:" << refs[r].GetFileOffset();
				osmscout::AreaRef area;
				if (_database->GetAreaByOffset(refs[r].GetFileOffset(), area)) {
					GetObject(area->GetFeatureValueBuffer(), coordinates, bbox);
					area->GetCenter(coordinates);
				}
			}
		}

		qDebug() << "Coord " << coordinates.GetLat() << " "  << coordinates.GetLon();

		printCount++;
		LocationEntry resultEntry(QString::fromUtf8(it->first.c_str()), coordinates, bbox);
		resultText.append(resultEntry);
	}

	return printCount;
}

std::string LibOsmScoutHelper::GetObject(const osmscout::FeatureValueBuffer& features,
	osmscout::GeoCoord& coordinates,
	osmscout::GeoBox& bbox)
{
	qDebug() << "   - type:     " << features.GetType()->GetName().c_str();

	for (auto featureInstance : features.GetType()->GetFeatures()) {
		if (features.HasFeature(featureInstance.GetIndex())) {
			auto feature = featureInstance.GetFeature();

			if (feature->HasValue() && feature->HasLabel()) {
				auto value = features.GetValue(featureInstance.GetIndex());
				if (value->GetLabel().empty()) {
					qDebug() << "   + feature " << feature->GetName().c_str();
				}
				else {
					qDebug() << "   + feature " << feature->GetName().c_str() << ": " << osmscout::UTF8StringToLocaleString(value->GetLabel()).c_str();
				}
			}
			else {
				qDebug() << "   + feature " << feature->GetName().c_str();
			}
		}
	}

	return "";
}

void LibOsmScoutHelper::MoveMap(int diffx, int diffy) {
	if(!_projection.Move(diffx, diffy)) {
		SomeThingWrongException exp("move map failed");
		exp.raise();
	}
	_center = _projection.GetCenter();

	UpdateMapData();
}

bool LibOsmScoutHelper::ZoomUp() {
	if (_zoomLevel + 1 < 21) {
		_zoomLevel++;
		_magnification.SetLevel(_zoomLevel);
		_projection.Set(_center,
			_magnification,
			_dpi,
			_width,
			_height);
		UpdateMapData();
		return true;
	}
	return false;
}

bool LibOsmScoutHelper::ZoomDown() {
	if (_zoomLevel - 1 > -1) {
		_zoomLevel--;
		_magnification.SetLevel(_zoomLevel);
		_projection.Set(_center,
			_magnification,
			_dpi,
			_width,
			_height);
		UpdateMapData();
		return true;
	}
	return false;
}

void LibOsmScoutHelper::CenterMap(const osmscout::GeoCoord& coord, bool paintMarker, const double& compass) {
	_center = coord;
		//Todo Check this !!
	
	if (compass > -1) {
		//double angleDeg = (360*(angle/(2*M_PI))); found in code
		// full turn is 2*PI
		_compass = -compass * (2.0 * M_PI) / 360.0;
		auto angleDeg = 360.0 * (_compass / (2.0 * M_PI));
		qDebug() << "Rotate to " << angleDeg << " Drive to " << compass;
	}
	_projection.Set(_center,
		_compass,
		_magnification,
		_dpi,
		_width,
		_height);

	if (compass > -1) {
		_markerPos = _projection.GetCenter();
		_projection.MoveUp(_height / 3);
	}
	_paintMarker = paintMarker;
	UpdateMapData();
}

void LibOsmScoutHelper::RouteFormTo(const osmscout::GeoCoord& start, const osmscout::GeoCoord& target) {

	//Mini Test Route
	//Coord  50.0908   8.50699
	//Coord  50.094   8.49617

	_lastRoute.clear();
	ShowMapInBox(start, target);
	
	osmscout::FastestPathRoutingProfile routingProfile(_database->GetTypeConfig());
	auto typeConfig = _database->GetTypeConfig();
	osmscout::RouterParameter           routerParameter;
	std::string                         routerFilenamebase = osmscout::RoutingService::DEFAULT_FILENAME_BASE;
	auto vehicle = osmscout::vehicleCar;

	qDebug() << "Database " << routerFilenamebase.c_str();

	routerParameter.SetDebugPerformance(true);

	auto router = std::make_shared<osmscout::SimpleRoutingService>(
		_database,
		routerParameter,
		routerFilenamebase);

	if (!router->Open()) {
		qDebug() << "Cannot open routing database";
		SomeThingWrongException exp("Cannot open routing database");
		exp.raise();
		return;
	}

	osmscout::RoutingParameter          parameter;
	std::map<std::string, double>        carSpeedTable;
	parameter.SetProgress(std::make_shared<QtRoutingProgress>());

	switch (vehicle) {
	case osmscout::vehicleFoot:
		routingProfile.ParametrizeForFoot(*typeConfig, 5.0);
		break;
	case osmscout::vehicleBicycle:
		routingProfile.ParametrizeForBicycle(*typeConfig, 20.0);
		break;
	case osmscout::vehicleCar:
		GetCarSpeedTable(carSpeedTable);
		routingProfile.ParametrizeForCar(*typeConfig,
			carSpeedTable,
			130.0);
		break;
	}

	qDebug() << "search start point for " << start.GetLat() << " " << start.GetLon();

	auto radius = 1000.0;
	auto startPosition = router->GetClosestRoutableNode(start, routingProfile, radius);
	if (!startPosition.IsValid()) {
		qDebug() << "Error while searching for routing node near start location!";
		SomeThingWrongException exp("Error while searching for routing node near start location!");
		exp.raise();
	}

	if (startPosition.GetObjectFileRef().GetType()==osmscout::refNode) {
		qDebug() << "Cannot find start node for start location!";
		SomeThingWrongException exp("Cannot find start node for start location!");
		exp.raise();
	}
	//Reset search radius
	radius = 10000.0;
	auto targetPosition = router->GetClosestRoutableNode(target, routingProfile, radius);
	if (!targetPosition.IsValid()) {
		qDebug() << "Error while searching for routing node near start location!";
		SomeThingWrongException exp("Error while searching for routing node near start location!");
		exp.raise();
	}

	if (targetPosition.GetObjectFileRef().GetType() == osmscout::refNode) {
		qDebug() << "Cannot find start node for start location!";
		SomeThingWrongException exp("Cannot find start node for start location!");
		exp.raise();
	}

	osmscout::RoutingResult result;
	try {
		result = router->CalculateRoute(routingProfile,
			startPosition,
			targetPosition,
			parameter);
	}
	catch(std::exception& exp) {
		qDebug() << exp.what();
		SomeThingWrongException exp2(exp.what());
		exp2.raise();
	}

	if (!result.Success()) {
		qDebug() << "There was an error while calculating the route!";
		router->Close();
		SomeThingWrongException exp("There was an error while calculating the route!");
		exp.raise();
		return;
	}

	qDebug() << "Route raw data:";
	for (const auto &entry : result.GetRoute().Entries()) {
		qDebug() << entry.GetPathObject().GetName().c_str() << "[" << entry.GetCurrentNodeIndex() << "]" << " = " << entry.GetCurrentNodeId() << " => " << entry.GetTargetNodeIndex();
	}

	osmscout::RouteDescription          description;
	router->TransformRouteDataToRouteDescription(result.GetRoute(),	description);

	std::list<osmscout::Point> points;
	if (!router->TransformRouteDataToPoints(result.GetRoute(), points)) {
		qDebug() << "Error during route conversion to points";
	} else {
		for (const auto &point : points) {
			_lastRoute.push_back(osmscout::GeoCoord(point.GetLat(), point.GetLon()));
		}
	}

	router->Close();
	RenderBaseMap();
}

void LibOsmScoutHelper::ShowMapInBox(const osmscout::GeoCoord& posOne, const osmscout::GeoCoord& posTwo) {
	auto box = osmscout::GeoBox(posOne, posTwo);
	_center = box.GetCenter();
	auto posMin = box.GetMinCoord();
	auto posMax = box.GetMaxCoord();

	//auto dimension = osmscout::GetEllipsoidalDistance(box.GetMinCoord(), box.GetMaxCoord());
	//Map is two big
	//auto mag = magnificationByDimension(dimension);

	auto found = false;
	//do it the stuped way
	for(auto i = 20; i >= 0; i--) {
		_magnification.SetLevel(i);
		_projection.Set(_center,
			_magnification,
			_dpi,
			_width,
			_height);
		if(_projection.IsValidFor(posOne) && _projection.IsValidFor(posTwo)) {
			double xOne, yOne, xTwo, yTwo;
			_projection.GeoToPixel(osmscout::GeoCoord(posMin.GetLat(), posMin.GetLon()), xOne, yOne);
			_projection.GeoToPixel(osmscout::GeoCoord(posMax.GetLat(), posMax.GetLon()), xTwo, yTwo);
			qDebug() << "xOne " << xOne << "yOne " << yOne << "xTwo " << xTwo << "yTwo " << yTwo;
			if (PosOnScreen(xOne,yOne) && PosOnScreen(xTwo,yTwo))
			{
				_zoomLevel = i;
				qDebug() << "Zoom level Found at " << _zoomLevel;
				found = true;
				break;
			}
		}
	}

	if(found) {
		UpdateMapData();
	}
	
}

osmscout::GeoCoord LibOsmScoutHelper::GetCoordFromPixel(const QPoint& point) {
	double xOne, yOne;
	_projection.PixelToGeo(point.x(), point.y(), xOne, yOne);
	return osmscout::GeoCoord(yOne, xOne);
}

void LibOsmScoutHelper::RegisterCallback(MapImageUpdateDelegate update, MapImageFinishDelegate finish) {
	_MapImageUpdate = update;
	_MapImageFinish = finish;
}

osmscout::Magnification LibOsmScoutHelper::magnificationByDimension(double dimension)
{
	osmscout::Magnification::Mag mag = osmscout::Magnification::magBlock;
	if (dimension > 0.1)
		mag = osmscout::Magnification::magVeryClose;
	if (dimension > 0.2)
		mag = osmscout::Magnification::magCloser;
	if (dimension > 0.5)
		mag = osmscout::Magnification::magClose;
	if (dimension > 1)
		mag = osmscout::Magnification::magDetail;
	if (dimension > 3)
		mag = osmscout::Magnification::magSuburb;
	if (dimension > 7)
		mag = osmscout::Magnification::magCity;
	if (dimension > 15)
		mag = osmscout::Magnification::magCityOver;
	if (dimension > 30)
		mag = osmscout::Magnification::magProximity;
	if (dimension > 60)
		mag = osmscout::Magnification::magRegion;
	if (dimension > 120)
		mag = osmscout::Magnification::magCounty;
	if (dimension > 240)
		mag = osmscout::Magnification::magStateOver;
	if (dimension > 500)
		mag = osmscout::Magnification::magState;
	return osmscout::Magnification(mag);
}

bool LibOsmScoutHelper::PosOnScreen(double x, double y) {
	if(x > 0.0 && y > 0.0) {
		if(x < static_cast<double>(_width) && y < static_cast<double>(_height)) {
			return true;
		}
	}
	return false;
}

void LibOsmScoutHelper::ClearUnusedData() {
	_data.ClearDBData();
}

bool LibOsmScoutHelper::TryGetNameAndSpeedLimit(const osmscout::FeatureValueBuffer& buffer, std::string& name, int& maxSpeed) {
	name = "";
	maxSpeed = -1;
	auto found = false;
	for (size_t idx = 0; idx < buffer.GetFeatureCount(); idx++) {
		auto meta = buffer.GetFeature(idx);

		if (buffer.HasFeature(idx)) {
			if (meta.GetFeature()->HasValue()) {
				auto value = buffer.GetValue(idx);
				if (dynamic_cast<osmscout::NameFeatureValue*>(value) != nullptr) {
					auto nameValue = dynamic_cast<osmscout::NameFeatureValue*>(value);
					name = nameValue->GetName();
					found = true;
				}
				else if (dynamic_cast<osmscout::NameAltFeatureValue*>(value) != nullptr) {
					if(name.size() == 0) {
						auto nameAltValue = dynamic_cast<osmscout::NameAltFeatureValue*>(value);
						name = nameAltValue->GetNameAlt();
						found = true;
					}
					
				}
				else if (dynamic_cast<osmscout::MaxSpeedFeatureValue*>(value) != nullptr) {
					auto maxSpeedValue = dynamic_cast<osmscout::MaxSpeedFeatureValue*>(value);
					maxSpeed = static_cast<int>(maxSpeedValue->GetMaxSpeed());
				}
			}
		}
	}
	return found;
}

bool LibOsmScoutHelper::FindWayForCoord(const osmscout::GeoCoord& coord, QString& name) {
	name = "";

	qDebug() << "FindWayForCoord for " << coord.GetDisplayText().c_str();

	auto typeConfig = _database->GetTypeConfig();
	
	if (!typeConfig) {
		return false;
	}

	std::vector<osmscout::WayRef> candidates;

	osmscout::TypeInfoSet wayTypes;

	// near addressable ways
	// type->CanRouteCar() &&
	for (const auto& type : typeConfig->GetTypes()) {
		if (type->CanBeWay() &&
			type->HasFeature(osmscout::NameFeature::NAME)) {
			wayTypes.Set(type);
		}
	}

	if (wayTypes.Empty()) {
		return false;
	}

	auto locationService = std::make_shared<osmscout::LocationDescriptionService>(_database);

	if (!locationService->LoadNearWays(coord, wayTypes, candidates, 10)) {
		return false;
	}

	if(candidates.size() == 0) {
		qDebug() << "Nothing found";
		return false;
	}

	auto found = false;
	for (auto it = candidates.begin(); it != candidates.end(); ++it) {
		const auto buffer = it->get()->GetFeatureValueBuffer();
		std::string nameIntern;
		int maxSpeed;
		if (TryGetNameAndSpeedLimit(buffer, nameIntern, maxSpeed)) {
			qDebug() << nameIntern.c_str() << " maxSpeed " << maxSpeed;
			if(name.size() == 0) {
				name = QString::fromUtf8(nameIntern.c_str());
				found = true;
			}
		} else {
			qDebug() << "Strasse ohne Namen";
		}
	}

	return found;
}

bool LibOsmScoutHelper::FindWayForCenter(QString& name) {
	return FindWayForCoord(_center, name);
}

osmscout::GeoCoord LibOsmScoutHelper::GetCenter() const {
	return _center;
}

int LibOsmScoutHelper::GetZoomLevel() const {
	return _zoomLevel;
}

void LibOsmScoutHelper::ToogleDayNight() {
	_stylesheetFlags["daylight"] = !_stylesheetFlags["daylight"];
	OpenStyle(_sytleFile);
	delete _mapPainter;
	_mapPainter = new osmscout::MapPainterQt(_styleConfig);
	if (_markerImage != nullptr)
	{
		delete _markerImage;
		_markerImage = nullptr;
	}
	RenderBaseMap();
}
