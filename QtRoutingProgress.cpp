﻿#include "stdafx.h"
#include "QtRoutingProgress.h"

QtRoutingProgress::QtRoutingProgress(): 
	lastDump(std::chrono::system_clock::now()), 
	maxPercent(0.0) {

}

void QtRoutingProgress::Reset() {
	lastDump = std::chrono::system_clock::now();
	maxPercent = 0.0;
}

void QtRoutingProgress::Progress(double currentMaxDistance, double overallDistance) {
	auto currentPercent = (currentMaxDistance*100.0) / overallDistance;
	auto now = std::chrono::system_clock::now();
	maxPercent = std::max(maxPercent, currentPercent);

	if (std::chrono::duration_cast<std::chrono::milliseconds>(now - lastDump).count()>50) {
		qDebug() << (size_t)maxPercent << "%";

		lastDump = now;
	}
}
