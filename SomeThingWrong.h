﻿#pragma once

#include <QtConcurrent/QtConcurrent>

class SomeThingWrongException : public QException {
	QString _message;
public:
	void raise() const override;
	SomeThingWrongException *clone() const override;
	explicit SomeThingWrongException(QString message);
	char const* what() const override;
};
