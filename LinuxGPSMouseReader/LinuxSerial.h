#ifndef SRC_LINUXSERIAL
#define SRC_LINUXSERIAL

#include <string>
#include <termios.h>
#include <signal.h>

class LinuxSerial
{
    int _handle;
    std::string _portname;
    termios _oldtio;
    termios _newtio;
    struct sigaction _saio;  
public:
    LinuxSerial(const std::string& portname);
    ~LinuxSerial();
    bool Open();
    void Close();
    int ReadData(char*& buffer);
};

#endif // SRC_LINUXSERIAL
