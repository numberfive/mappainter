#include <iostream>
#include <cstdlib>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions
#include "LinuxSerial.h"

//https://en.wikibooks.org/wiki/Serial_Programming/termios
//http://www.tldp.org/HOWTO/Serial-Programming-HOWTO/x115.html

/***************************************************************************
* signal handler. sets wait_flag to FALSE, to indicate above loop that     *
* characters have been received.                                           *
***************************************************************************/
bool wait_flag = true;

void signal_handler_IO (int status) {
    wait_flag = false;
}
      
LinuxSerial::LinuxSerial(const std::string& portname) {
    _portname = portname;
    _handle = -1;
}

LinuxSerial::~LinuxSerial()
{
}

bool LinuxSerial::Open() {
    _handle = open(_portname.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
    
    if (_handle == -1) {
        std::cout << "Error " << errno << " opening " << _portname << ": " << strerror (errno) << std::endl;
        return false;
    }
    tcgetattr(_handle,&_oldtio); /* save current port settings */
    
    /* install the signal handler before making the device asynchronous */
    _saio.sa_handler = signal_handler_IO;
    //Set to empty the sa_mask. It means that no signal is blocked
    sigemptyset(&_saio.sa_mask);
    _saio.sa_flags = 0;
    _saio.sa_restorer = NULL;
    sigaction(SIGIO,&_saio,NULL);
    
    /* allow the process to receive SIGIO */
    fcntl(_handle, F_SETOWN, getpid());
    fcntl(_handle, F_SETFL, FASYNC);
    
    return true;
}

void LinuxSerial::Close() {
    if(_handle != -1) {
        /* restore old port settings */
        tcsetattr(_handle,TCSANOW,&_oldtio);
        close(_handle);
    }
}

int LinuxSerial::ReadData(char*& buffer) {
    if(wait_flag) return 0;
    buffer = new char[255];
    return read(_handle,buffer,255);
}