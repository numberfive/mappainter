#include <stdio.h>
#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string.h>  
#include "LinuxSerial.h"

bool running = true;

void my_handler(int s){
   running = false;
}

int main(int argc, char **argv)
{
    auto port = new LinuxSerial("/dev/ttyUSB0");
    if(!port->Open()) {
        printf("failed to open port\n");
        return -1;
    }
    
    //enable ctrl +c
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    if(sigaction(SIGINT, &sigIntHandler, NULL) == -1){
        std::cout << "Error " << errno << " " << strerror (errno) << std::endl;
        return -2;
    }
    int ch = '*';
    std::ofstream examplefile ("log.txt", std::ios::out | std::ios::app);
    while(running){
        ch = std::getchar();
        if(ch == 'q') {
            running = false;
            break;
        }
        char* buffer;
        auto bytes = port->ReadData(buffer);
        if( bytes == 0){
            usleep(10000);
        } else {
            if(bytes == 1 && buffer[0] == 10) continue;
            
            printf(":%.*s:%d", bytes, buffer, bytes);
            if (examplefile.is_open()) {
                auto text = std::string(buffer, bytes);
                examplefile << text;
            }
            delete [] buffer;
        }
    }
    
    port->Close();
    if (examplefile.is_open()) {
        examplefile.close();
    }
    delete port;
    return 0;
}
