﻿#pragma once

#include "ui_GPSReceiver.h"
#include <osmscout/GeoCoord.h>
#include "task.h"

typedef std::function<void(const osmscout::GeoCoord& coord, const double& compass)> MapPosUpdateDelegate;
typedef std::function<void(const double& speed)> SpeedUpdateDelegate;


class GPSReceiverWindow : public QDialog {
	Q_OBJECT
	bool _closeDialog;
public:
	explicit GPSReceiverWindow(QWidget *parent);
	void RegisterCallback(MapPosUpdateDelegate updateMap, SpeedUpdateDelegate updateSpeed);
	void closeEvent(QCloseEvent *event) override;
	
private:
	Ui::GPSReceiverClass ui;
	MapPosUpdateDelegate _updateFunc;
	SpeedUpdateDelegate _updateSpeedFunc;
	void MoveMapTo(const osmscout::GeoCoord& coord, const double& compass);
	void SetSpeedTo(double speed);
	void StartNmeaFileReader(const QString& filename);
	void startButtonClicked(bool checked = false);
	void FileFinish();
};
