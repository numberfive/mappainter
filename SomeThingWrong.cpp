﻿#include "stdafx.h"
#include "SomeThingWrong.h"
#include <QDebug>

void SomeThingWrongException::raise() const {
	qDebug() << "\nException: ";
	throw *this;
}

SomeThingWrongException* SomeThingWrongException::clone() const {
	 return new SomeThingWrongException(*this);
}

SomeThingWrongException::SomeThingWrongException(QString message) {
	_message = message;
}

char const* SomeThingWrongException::what() const {
	return _message.toUtf8();
}
