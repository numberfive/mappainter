﻿#pragma once
#include <osmscout/routing/RoutingService.h>

class QtRoutingProgress : public osmscout::RoutingProgress {
	std::chrono::system_clock::time_point lastDump;
	double                                maxPercent;
public:
	QtRoutingProgress();
	void Reset() override;
	void Progress(double currentMaxDistance, double overallDistance) override;
};
