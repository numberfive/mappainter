﻿#pragma once
#include "LibOsmScoutHelper.h"

class MapWidget : public QWidget {
	Q_OBJECT
	LibOsmScoutHelper* _libOsmScout;
	QPixmap _currentMap;
	bool updateRunning;
	QPoint _mouseDownAt;
	QPoint _mapMove;
	QPoint _mapMoveThreadStart;
	QThread* _guithread;
	std::mutex _mapDataLock;

	void ContextMenuRequested(const QPoint &pos);
	void SetRouteStart();
	void SetRouteTarget();

signals:
	void updateStatusLineSignal(const QString& text);
	void setRouteStartSignal(const osmscout::GeoCoord& position);
	void setRouteTargetSignal(const osmscout::GeoCoord& position);

public:
	explicit MapWidget(QWidget *parent = nullptr);
	bool event(QEvent *evt) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent * event) Q_DECL_OVERRIDE;
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent *event) override;
	void MoveMap(int diffx, int diffy);
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;

	void SetMapHelper(LibOsmScoutHelper* libOsmScout);
	void UpdateMapImage(const QImage &image);
	void UpdateMapImageFinish();
	void RealFinish();
	void CenterMap(const osmscout::GeoCoord& coord, const double& compass);
};
