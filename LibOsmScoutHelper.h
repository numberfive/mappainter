﻿#pragma once

#include <osmscout/Database.h>
#include <osmscout/MapService.h>
#include <osmscout/MapPainterQt.h>
#include <osmscout/LocationService.h>
#include "LocationEntry.h"
#include <qimage.h>

typedef std::function<void(const QImage &image)> MapImageUpdateDelegate;
typedef std::function<void()> MapImageFinishDelegate;

class LibOsmScoutHelper {
	osmscout::DatabaseRef         _database;
	osmscout::MapServiceRef       _mapService;
	osmscout::StyleConfigRef      _styleConfig;
	osmscout::MapParameter        _drawParameter;
	osmscout::MapPainterQt*		  _mapPainter;
	osmscout::MercatorProjection  _projection;
	osmscout::MapData             _data;
	osmscout::GeoCoord            _center;
	osmscout::Magnification       _magnification;
	std::list<osmscout::TileRef>  _tiles;
	std::vector<osmscout::GeoCoord> _lastRoute;
	QString                         _databaseDirectory;
	QString                         _sytleFile;
	osmscout::MapService::CallbackId _titleCallBackId;
	QImage _currentMap;
	std::mutex                       _mapDataLock;
	int _zoomLevel;
	qreal _dpi;
	int _width;
	int _height;
	bool _firstloadDone;
	bool _paintMarker;
	double _compass;
	bool _useMarkerImage;
	osmscout::GeoCoord            _markerPos;
	QImage*                       _markerImage;
	std::unordered_map<std::string, bool> _stylesheetFlags;
	MapImageUpdateDelegate _MapImageUpdate;
	MapImageFinishDelegate _MapImageFinish;

	std::string LibOsmScoutHelper::GetObject(const osmscout::FeatureValueBuffer& features,
		osmscout::GeoCoord& coordinates,
		osmscout::GeoBox& bbox);
	osmscout::Magnification magnificationByDimension(double dimension);
	bool PosOnScreen(double x, double y);
	void ClearUnusedData();
	bool TryGetNameAndSpeedLimit(const osmscout::FeatureValueBuffer& buffer, std::string& name, int& maxSpeed);
public:

	LibOsmScoutHelper();
	
	void OpenDatabase(const QString& directory);
	void OpenStyle(const QString& char_);
	void BuildingMap(osmscout::GeoCoord center, int zoomLevel, const std::vector<std::string>& iconPath);
	void RenderOnPainter(QPainter* painter) const;
	void RenderBaseMap();
	void UpdateSize(const QSize& size);
	void UpdateMapData();
	void UpdateSize(int width, int height);
	size_t SearchFor(const QString& text, QList<LocationEntry>& resultText);
	void MoveMap(int diffx, int diffy);
	bool ZoomUp();
	bool ZoomDown();
	void CenterMap(const osmscout::GeoCoord& coord, bool paintMarker, const double& compass);
	void RouteFormTo(const osmscout::GeoCoord& start, const osmscout::GeoCoord& target);
	void ShowMapInBox(const osmscout::GeoCoord& posOne, const osmscout::GeoCoord& posTwo);
	osmscout::GeoCoord GetCoordFromPixel(const QPoint& point);
	void RegisterCallback(MapImageUpdateDelegate update, MapImageFinishDelegate finish);
	bool FindWayForCoord(const osmscout::GeoCoord& coord, QString& name);
	bool FindWayForCenter(QString& name);
	osmscout::GeoCoord GetCenter() const;
	int GetZoomLevel() const;
	void ToogleDayNight();
};
