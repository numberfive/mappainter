﻿#include "stdafx.h"
#include "LocationEntry.h"

LocationEntry::LocationEntry() {
}

LocationEntry::LocationEntry(QString label, osmscout::GeoCoord coord, osmscout::GeoBox bbox) {
	_label = label;
	_coord = coord;
	_bbox = bbox;
}

QString LocationEntry::Label() {
	return _label;
}

osmscout::GeoCoord LocationEntry::Coord() {
	return _coord;
}

osmscout::GeoBox LocationEntry::Box() {
	return _bbox;
}
