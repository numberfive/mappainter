#include "stdafx.h"
#include "QtMapPainterWithLibOsmScout.h"
#include "MapWidget.h"
#include "task.h"
#include "SomeThingWrong.h"
#include "GPSReceiverWindow.h"

bool QtMapPainterWithLibOsmScout::event(QEvent* event) {
	if (event->type() == QEvent::Show) {
		//I dont understand we have todo this loading settings in ctor alwasys giv the deflaut value why ?
		if (_mySettings.isNull()) LoadSettings();
	}
	return QMainWindow::event(event);
}

QtMapPainterWithLibOsmScout::QtMapPainterWithLibOsmScout(QWidget *parent)
	: QMainWindow(parent)
{
	_gpsReceiverUi = nullptr;

	ui.setupUi(this);

	setWindowTitle("Show an Map with OSMScout");
	setObjectName("MainWindow");

	_libOsmScout = new LibOsmScoutHelper();

	statusBar()->showMessage(tr("Idle"));

	connect(ui.actionopendatabase, &QAction::triggered, this, &QtMapPainterWithLibOsmScout::OpenDatabase);
	connect(ui.actionSavePicture, &QAction::triggered, this, &QtMapPainterWithLibOsmScout::SavePicture);
	connect(ui.actionAppInfos, &QAction::triggered, this, &QtMapPainterWithLibOsmScout::AppInfos);
	connect(ui.actionStartGPSReceiver, &QAction::triggered, this, &QtMapPainterWithLibOsmScout::StartGPSReceiver);
	connect(ui.actionOpenLastMap, &QAction::triggered, this, &QtMapPainterWithLibOsmScout::OpenLastMap);
	connect(ui.actionNacht_Modus, &QAction::triggered, this, &QtMapPainterWithLibOsmScout::SwitchDayNight);

	_mapWidget = new MapWidget(ui.horizontalLayoutWidget);

	//Das ist sicher nicht das wie QT Programiere es machen w�rden
	//Da muss es was besseres geben
	ui.horizontalLayout->removeWidget(ui.mapParent);
	ui.horizontalLayout->insertWidget(0, _mapWidget);

	connect(_mapWidget, &MapWidget::updateStatusLineSignal, this, &QtMapPainterWithLibOsmScout::updateStatusLine);
	connect(_mapWidget, &MapWidget::setRouteStartSignal, this, &QtMapPainterWithLibOsmScout::setRouteStart);
	connect(_mapWidget, &MapWidget::setRouteTargetSignal, this, &QtMapPainterWithLibOsmScout::setRouteTarget);
	connect(ui.searchlineEdit, &QLineEdit::textChanged, this, &QtMapPainterWithLibOsmScout::updateSearchText);
	connect(ui.searchpushButton, &QPushButton::clicked, this, &QtMapPainterWithLibOsmScout::searchButtonClicked);
	connect(ui.routepushButton, &QPushButton::clicked, this, &QtMapPainterWithLibOsmScout::routeButtonClicked);
	
	
	//User Widget an den Innen bereich des Hauptfensters binden
	//setCentralWidget(_mapWidget);

	//Todo https://wiki.qt.io/PushButton_Based_On_Action

	ui.waitinglabel->hide();
	_movieWaiting = new QMovie(":/QtMapPainterWithLibOsmScout/Resources/loader.gif");
	ui.waitinglabel->setMovie(_movieWaiting);

	//Why get hier always the default value ?!?!?
	/*QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName(), parent);
	_databasePath = settings.value("map/Database").toString();
	if(_databasePath.size() == 0) {
		_databasePath = "D:/Mine/OpenSource/libosmscout-code";
	}
	_stylePath = settings.value("map/stylePath", "D:/Mine/OpenSource/libosmscout-code").toString();
	_styleLast = settings.value("map/style", "").toString();
	*/

	//To Use the class in Variant
	_typId = qRegisterMetaType<LocationEntry>("LocationEntry");
	ui.searchResultListWidget->setContextMenuPolicy(Qt::CustomContextMenu);

	connect(ui.searchResultListWidget, &QListWidget::customContextMenuRequested, this, &QtMapPainterWithLibOsmScout::searchListMenu);

	ui.routeStartLatLineEdit->setText("50.0908");
	ui.routeStartLonLineEdit->setText("8.50699");

	ui.actionOpenLastMap->setEnabled(false);
}

QtMapPainterWithLibOsmScout::~QtMapPainterWithLibOsmScout() {
	if(_libOsmScout != nullptr) {
		delete _libOsmScout;
	}
}

void QtMapPainterWithLibOsmScout::closeEvent(QCloseEvent* event) {
	qDebug("save settings");

	if (!_mySettings.isNull())
	{
		_mySettings->setValue("map/Database", _databasePath);
		_mySettings->setValue("map/stylePath", _stylePath);
		_mySettings->setValue("map/style", _styleLast);
		if(_libOsmScout) {
			auto center = _libOsmScout->GetCenter();
			auto level = _libOsmScout->GetZoomLevel();
			_mySettings->setValue("map/zoomLevel", level);
			_mySettings->setValue("map/posLat", center.GetLat());
			_mySettings->setValue("map/posLon", center.GetLon());
		}
		_mySettings->sync();

		if (_mySettings->status() != QSettings::NoError) {
			qDebug("error save setting");
		}
	} else {
		qDebug("settings pointer is null");
	}

	event->accept();
	//event->ignore();
}

void QtMapPainterWithLibOsmScout::actionTriggered(QAction *action)
{
	qDebug("action '%s' triggered", action->text().toLocal8Bit().data());
}

void QtMapPainterWithLibOsmScout::OpenDatabase() {
	qDebug("Open the Database");
		
	QFileDialog dialog(this);
	dialog.setDirectory(_databasePath);
	dialog.setFileMode(QFileDialog::DirectoryOnly);
	dialog.setWindowTitle(QString::fromLatin1("Datenbank ausw�hlen"));
	if (!dialog.exec()) {
		statusBar()->showMessage(tr("Open Database canceled"));
		return;
	}
	auto files = dialog.selectedFiles();
	if (files.size() == 1) {
		statusBar()->showMessage(tr("Loading Database ") + files[0]);
		_libOsmScout->OpenDatabase(files[0]);
		_databasePath = QDir(files[0]).absolutePath();
	}

	dialog.setDirectory(_stylePath);
	dialog.setWindowTitle(QString::fromLatin1("Style ausw�hlen"));
	dialog.setFileMode(QFileDialog::ExistingFile);
	dialog.setNameFilter(tr("OSMStyle (*.oss)"));
	if (!dialog.exec()) {
		statusBar()->showMessage(tr("Open Style canceled"));
		return;
	}

	files = dialog.selectedFiles();
	if (files.size() == 1) {
		statusBar()->showMessage(tr("Loading Style ") + files[0]);
		_libOsmScout->OpenStyle(files[0]);
		QFileInfo fileInfo(files[0]);
		_stylePath = fileInfo.absolutePath();
		_styleLast = files[0];
	}

	dialog.setDirectory(_iconPath);
	dialog.setWindowTitle(QString::fromLatin1("Icon Pfad ausw�hlen"));
	dialog.setFileMode(QFileDialog::DirectoryOnly);
	if (!dialog.exec()) {
		statusBar()->showMessage(tr("Open Icon canceled"));
		return;
	}

	files = dialog.selectedFiles();
	if (files.size() == 1) {
		QFileInfo fileInfo(files[0]);
		_iconPath = fileInfo.absolutePath();
	}

	statusBar()->showMessage(tr("Building the map"));

	//Show Map with this data
	std::vector<std::string> iconPaths;
	iconPaths.push_back(_iconPath.toStdString());
	_libOsmScout->BuildingMap(_lastCenter, _lastLevel, iconPaths);

	ui.actionSavePicture->setEnabled(true);
	ui.searchlineEdit->setEnabled(true);

	_mapWidget->SetMapHelper(_libOsmScout);
}

void QtMapPainterWithLibOsmScout::OpenLastMap() const {

	qDebug("Open the Database");
	_libOsmScout->OpenDatabase(_databasePath);
	_libOsmScout->OpenStyle(_styleLast);
	std::vector<std::string> iconPaths;
	iconPaths.push_back(_iconPath.toStdString());
	_libOsmScout->BuildingMap(_lastCenter, _lastLevel, iconPaths);

	ui.actionSavePicture->setEnabled(true);
	ui.searchlineEdit->setEnabled(true);

	_mapWidget->SetMapHelper(_libOsmScout);
}

void QtMapPainterWithLibOsmScout::SwitchDayNight() {
	if(_libOsmScout == nullptr) {
		return;
	}

	_libOsmScout->ToogleDayNight();
}

void QtMapPainterWithLibOsmScout::SavePicture() {
	//Todo This Funktion Failed when MapWidget hat resized the map
	//Todo Move to _mapWidget
	qDebug("SavePicture");

	auto pixmap = new QPixmap(640, 480);
	auto painter = new QPainter(pixmap);
	_libOsmScout->RenderOnPainter(painter);

	if (!pixmap->save("map.png", "PNG", -1)) {
		QMessageBox Msgbox(this);
		Msgbox.setText("Error saving picture");
		Msgbox.exec();
	} else {
		statusBar()->showMessage(tr("map saved as png"));
	}
}

void QtMapPainterWithLibOsmScout::AppInfos() {
	qDebug() << "AppInfos";
	QMessageBox::aboutQt(this);
}

void QtMapPainterWithLibOsmScout::StartGPSReceiver() {
	if(_gpsReceiverUi == nullptr) {
		_gpsReceiverUi = new GPSReceiverWindow(this);
		auto delegateUpdate = std::bind(&QtMapPainterWithLibOsmScout::UpdatePosFromGPSReceiver, this, std::placeholders::_1, std::placeholders::_2);
		auto delegateSpeedUpdate = std::bind(&QtMapPainterWithLibOsmScout::UpdateSpeedFromGPSReceiver, this, std::placeholders::_1);
		_gpsReceiverUi->RegisterCallback(delegateUpdate, delegateSpeedUpdate);
	}
	_gpsReceiverUi->show();
}

void QtMapPainterWithLibOsmScout::updateStatusLine(const QString& text) {
	//MapWidget Event 
	statusBar()->showMessage(text);
}

void QtMapPainterWithLibOsmScout::updateSearchText(const QString& text) {
	//Enable search when input is lager than 3 Chars (speed upserach)
	if(text.size() > 3) {
		ui.searchpushButton->setEnabled(true);
	} else {
		ui.searchpushButton->setEnabled(false);
	}
}

void QtMapPainterWithLibOsmScout::searchButtonClicked(bool checked) {
	SetUiToWaitingMode();
	ui.searchResultListWidget->clear();
	statusBar()->showMessage(tr("searching..."));
	_searchText = ui.searchlineEdit->text();
	qDebug() << "search for " << _searchText;

	//Starting Task for search
	auto drawMap = [this]() { SearchTextInMap(); };
	Task::future<void>& drawTask = Task::run(drawMap);
	drawTask.then([this] { SetUiToNormalMode(); });
}

void QtMapPainterWithLibOsmScout::searchListMenu(const QPoint& pos) {
	QPoint globalPos = ui.searchResultListWidget->mapToGlobal(pos);
	auto item = ui.searchResultListWidget->currentItem();
	auto data = item->data(Qt::UserRole);

	if(data.isNull()) return;

	auto currentEntry = data.value<LocationEntry>();

	QMenu myMenu;
	myMenu.addAction("in Karte anzeigen", this, &QtMapPainterWithLibOsmScout::ShowSearchEntryOnMap);
	myMenu.exec(globalPos);
}

void QtMapPainterWithLibOsmScout::SetUiToWaitingMode() {
	ui.waitinglabel->show();
	_movieWaiting->start();
}

void QtMapPainterWithLibOsmScout::SetUiToNormalMode() {
	ui.waitinglabel->hide();
	_movieWaiting->stop();
}

void QtMapPainterWithLibOsmScout::SearchTextInMap() {
	//Attention this Funktion is called in background Thread (TASK Helper code)
	QList<LocationEntry> resultTextList;
	auto resultCount = _libOsmScout->SearchFor(_searchText, resultTextList);
	if (resultCount > 0) {
		for (auto &entry : resultTextList) {
			auto newItem = new QListWidgetItem();
			QVariant var;
			var.setValue(entry);
			newItem->setData(Qt::UserRole, var);
			newItem->setText(entry.Label());
			ui.searchResultListWidget->addItem(newItem);
		}
		auto resultText(QString::fromLatin1("%1 Eintr�ge gefunden"));
		resultText = resultText.arg(resultCount);
		ui.searchResultListWidget->addItem(resultText);
	}
	else {
		ui.searchResultListWidget->addItem("Nichts gefunden");
	}
}

void QtMapPainterWithLibOsmScout::LoadSettings() {

	_mySettings.reset(new QSettings());
	_databasePath = _mySettings->value("map/Database").toString();
	if (_databasePath.size() == 0) {
		_databasePath = "D:/Mine/OpenSource/libosmscout-code";
	}
	_stylePath = _mySettings->value("map/stylePath", "D:/Mine/OpenSource/libosmscout-code").toString();
	_styleLast = _mySettings->value("map/style", "").toString();
	_iconPath = _mySettings->value("map/iconPath", "D:/Mine/OpenSource/libosmscout-code/libosmscout/data/icons/14x14/standard/").toString();
	auto lastLat = _mySettings->value("map/posLat", "50.0956").toDouble(); 
	auto lastLon = _mySettings->value("map/posLon", "8.5096").toDouble();
	_lastCenter.Set(lastLat, lastLon);
	_lastLevel = _mySettings->value("map/zoomLevel", "15").toInt();
	
	if (_styleLast.size() != 0) {
		ui.actionOpenLastMap->setEnabled(true);
	};
}

void QtMapPainterWithLibOsmScout::ShowSearchEntryOnMap() {
	auto item = ui.searchResultListWidget->currentItem();
	auto data = item->data(Qt::UserRole);

	if (data.isNull()) return;

	//Todo move to _mapWidget
	auto currentEntry = data.value<LocationEntry>();
	_libOsmScout->CenterMap(currentEntry.Coord(), true, -1);
}

void QtMapPainterWithLibOsmScout::routeButtonClicked(bool checked) {
	auto startLat = ui.routeStartLatLineEdit->text().toDouble();
	auto startLon = ui.routeStartLonLineEdit->text().toDouble();
	auto targetLat = ui.routeTargetLatLineEdit->text().toDouble();
	auto targetLon = ui.routeTargetLonLineEdit->text().toDouble();

	statusBar()->showMessage(tr("routing..."));
	SetUiToWaitingMode();
	
	//Coord  50.0992   8.50445 Bechtenwald
	// 50.0908, 8.50699 Bahnhof
	// osmscout::GeoCoord(50.0908, 8.50699)
	// osmscout::GeoCoord(50.1141,8.68733) Zeil 51
	// osmscout::GeoCoord(50.07885, 8.50814)
	// osmscout::GeoCoord(52.33776, 14.54759) Bahnhof Franfurt Oder !!

	try {
		_libOsmScout->RouteFormTo(osmscout::GeoCoord(startLat, startLon), osmscout::GeoCoord(targetLat, targetLon));
	}
	catch(SomeThingWrongException& exp) {
		QMessageBox msgBox(this);
		msgBox.setText(QString::fromUtf8(exp.what()));
		msgBox.exec();
		SetUiToNormalMode();
		statusBar()->showMessage(tr("Error"));
		return;
	}
	
	SetUiToNormalMode();
	statusBar()->showMessage(tr("Idle"));
}

void QtMapPainterWithLibOsmScout::setRouteStart(const osmscout::GeoCoord& position) {
	ui.routeStartLatLineEdit->setText(QString::number(position.GetLat(), 10, 8));
	ui.routeStartLonLineEdit->setText(QString::number(position.GetLon(), 10, 8));
}

void QtMapPainterWithLibOsmScout::setRouteTarget(const osmscout::GeoCoord& position) {
	ui.routeTargetLatLineEdit->setText(QString::number(position.GetLat(), 10, 8));
	ui.routeTargetLonLineEdit->setText(QString::number(position.GetLon(), 10, 8));
}

void QtMapPainterWithLibOsmScout::DrawSpeedLabel() {
	auto text = QString::number(_lastSpeed, 10, 1);
	text += " km/h ";
	text += QString::number(_lastCompass, 10, 1);
	text += QString::fromLatin1(" �");
	ui.speedLabel->setText(text);
}

void QtMapPainterWithLibOsmScout::UpdatePosFromGPSReceiver(const osmscout::GeoCoord& coord, const double& compass) {
	_lastCompass = compass;
	DrawSpeedLabel();
	_mapWidget->CenterMap(coord, compass);
}

void QtMapPainterWithLibOsmScout::UpdateSpeedFromGPSReceiver(const double& speed) {
	_lastSpeed = speed;
	DrawSpeedLabel();
}
