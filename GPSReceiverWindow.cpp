﻿#include "stdafx.h"
#include "GPSReceiverWindow.h"
#include "task.h"
#include <fstream>
#include "NMEADecoder.h"
#include <osmscout/GeoCoord.h>
#include <complex>
#include <synchapi.h>

GPSReceiverWindow::GPSReceiverWindow(QWidget *parent):
	QDialog(parent){

	ui.setupUi(this);
	_closeDialog = false;
	connect(ui.pushButtonStart, &QPushButton::clicked, this, &GPSReceiverWindow::startButtonClicked);

}

void GPSReceiverWindow::RegisterCallback(MapPosUpdateDelegate updateMap, SpeedUpdateDelegate updateSpeed) {
	_updateFunc = updateMap;
	_updateSpeedFunc = updateSpeed;
}

void GPSReceiverWindow::closeEvent(QCloseEvent* event) {
	_closeDialog = true;
	Sleep(500);
}

void GPSReceiverWindow::MoveMapTo(const osmscout::GeoCoord& coord, const double& compass) {
	qDebug() << "move map to " << coord.GetDisplayText().c_str();
	if(_updateFunc) {
		_updateFunc(coord, compass);
	}
}

void GPSReceiverWindow::SetSpeedTo(double speed) {
	if (_updateSpeedFunc) {
		_updateSpeedFunc(speed);
	}
}

void GPSReceiverWindow::StartNmeaFileReader(const QString& filename) {
	std::ifstream file(filename.toStdString());
	std::string str;
	NMEADecoder decoder;
	osmscout::GeoCoord coordLast(-1,-1);

	while (std::getline(file, str))
	{
		if(decoder.Decode(str)) {
			qDebug() << "Line Read and decode";
			
			if(decoder.IsPositionValid()) {
				osmscout::GeoCoord coord(decoder.GetLatitude(), decoder.GetLongitude());
				qDebug() << "we have an position " << coord.GetDisplayText().c_str();
				if(coordLast.GetLat() == -1) {
					double compass = -1;
					if (decoder.IsCompassValid()) {
						compass = decoder.GetCompass();
						qDebug() << "drive to " << compass;
					}
					MoveMapTo(coord, compass);
					coordLast = coord;
				} else {
					//Distance in KM
					if (std::abs(coord.GetDistance(coordLast)) > 0.001) {
						double compass = -1;
						if(decoder.IsCompassValid()) {
							compass = decoder.GetCompass();
							qDebug() << "drive to " << compass;
						}
						MoveMapTo(coord, compass);
						coordLast = coord;
						Sleep(500); // This not work on Linux
					}
				}
			}
			if(decoder.IsSpeedValid()) {
				SetSpeedTo(decoder.GetSpeed());
			}
		}
		if(_closeDialog) {
			break;
		}
	}
}

void GPSReceiverWindow::startButtonClicked(bool checked) {
	_closeDialog = false; // I don't Understand why QT don't distroy this window
	if(ui.comboBoxNMEASource->currentIndex() == 0) {
		QFileDialog dialog(this);
		dialog.setWindowTitle(QString::fromLatin1("NMEA Log auswählen"));
		dialog.setFileMode(QFileDialog::ExistingFile);
		dialog.setNameFilter(tr("Textdatei (*.txt)"));

		if (!dialog.exec()) {
			return;
		}
		auto files = dialog.selectedFiles();
		auto filename = files[0];
		ui.pushButtonStart->setDisabled(true);
		ui.comboBoxNMEASource->setDisabled(true);
		auto& readNmeaTask = Task::run([this, filename] { StartNmeaFileReader(filename); });
		readNmeaTask.then([this] {FileFinish(); });
		
	} else {
		QMessageBox Msgbox(this);
		Msgbox.setText("Nicht Implementiert");
		Msgbox.exec();
	}

}

void GPSReceiverWindow::FileFinish() {
	ui.pushButtonStart->setDisabled(false);
	ui.comboBoxNMEASource->setDisabled(false);
}
