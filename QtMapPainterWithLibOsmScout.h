#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QtMapPainterWithLibOsmScout.h"
#include "LibOsmScoutHelper.h"
#include "MapWidget.h"
#include "GPSReceiverWindow.h"
#include <qsettings.h>

class QtMapPainterWithLibOsmScout : public QMainWindow
{
	Q_OBJECT

protected:
	bool event(QEvent *event) override;

public:
	explicit QtMapPainterWithLibOsmScout(QWidget *parent = Q_NULLPTR);
	~QtMapPainterWithLibOsmScout();
	void closeEvent(QCloseEvent *event) override;
	typedef QSharedPointer<QSettings> SettingsPtr;
	
public slots:
	void actionTriggered(QAction *action);

private:
	Ui::QtMapPainterWithLibOsmScoutClass ui;
	LibOsmScoutHelper* _libOsmScout;
	MapWidget* _mapWidget;
	QString _searchText;
	QString _databasePath;
	QString _stylePath;
	QString _styleLast;
	QString _iconPath;
	osmscout::GeoCoord _lastCenter;
	QMovie* _movieWaiting;
	SettingsPtr _mySettings;
	GPSReceiverWindow* _gpsReceiverUi;
	int _typId;
	int _lastLevel;
	double _lastSpeed;
	double _lastCompass;
	
	void OpenDatabase();
	void OpenLastMap() const;
	void SwitchDayNight();
	void SavePicture();
	void AppInfos();
	void StartGPSReceiver();
	void updateStatusLine(const QString& text);
	void updateSearchText(const QString& text);
	void searchButtonClicked(bool checked = false);
	void searchListMenu(const QPoint &pos);
	void SetUiToWaitingMode();
	void SetUiToNormalMode();
	void SearchTextInMap();
	void LoadSettings();
	void ShowSearchEntryOnMap();
	void routeButtonClicked(bool checked = false);
	void setRouteStart(const osmscout::GeoCoord& position);
	void setRouteTarget(const osmscout::GeoCoord& position);
	void DrawSpeedLabel();
	void UpdatePosFromGPSReceiver(const osmscout::GeoCoord& coord, const double& compass);
	void UpdateSpeedFromGPSReceiver(const double& speed);
};
